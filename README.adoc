= ✨webapp demo timeline lab
:toc: 
:sectnums:

il s'agit d'une webapp pour suivre l'avancement du lab.

_#responsive_ , _#materialize_

== ✅ prerequis

* utilise le broker MQTT https://maqiatto.com[maqiatto.com]
* les dépendances :
** https://github.com/mqttjs/MQTT.js[MQTT.js]
** https://vuejs.org/[Vue.js]
** https://materializecss.com/[MaterializeCSS]
** http://expressjs.com/[Express]
* le token `MQTT_TOKEN` est à remplacer dans le fichier __demo.html__

== 🛠 installation

.installation en local
```
node install
```

== 🚀 running

la webapp est la page timeline.html__

elle est accessible via un serveur web sous expressjs

.Running en local
```
node timeline.js
```
.URL WebApp :
http://localhost:8080/timeline.html


== 📖 Manuel


=== connection broker

au chargement de la webapp, la connection s'effectue automatiquement 

.badge connexion en cours
image::doc/badge_connecting.png[]

.badge connexion ok
image::doc/badge_connected.png[]

[source,javascript,indent=0]
----
include::timeline.html[lines=53..66]
----



quand la connection est ok : 

* un message est publié sur le topic `hello` (pour se déclarer)
* la webapp écoute alors sur topic `start`

.capture webapp au démarrage
image::doc/webapp_start.png[]



=== écoute le topic `start`

la webapp écoute le topic `start`

quand un message est publié sur ce topic, l'évenement _start_ de la timeline est affiché.

la webapp écoute alors sur topic `checkpoint1`

[source,javascript,indent=0]
----
include::timeline.html[lines=85..102]
----

.capture webapp sur reception d'un message *start*
image::doc/webapp_start_recu.png[]





=== écoute le topic `checkpoint1`

la webapp écoute le topic `checkpoint1`

quand un message est publié sur ce topic, l'évenement _checkpoint1_ de la timeline est affiché.

[source,javascript,indent=0]
----
include::timeline.html[lines=103..121]
----

.capture webapp sur reception d'un message *checkpoint1*
image::doc/webapp_checkpoint1_recu.png[]




=== écoute le topic `finish`

la webapp écoute le topic `finish`

quand un message est publié sur ce topic, l'évenement _finish_ de la timeline est affiché.

[source,javascript,indent=0]
----
include::timeline.html[lines=123..140]
----

.capture webapp sur reception d'un message *finish*
image::doc/webapp_finish_recu.jpg[]
