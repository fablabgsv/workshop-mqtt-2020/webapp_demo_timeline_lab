
var http = require('http');
var fs = require('fs');

var express = require('express'),
server = express(); 
server.use('/', express.static(__dirname + '/'));
server.use('/assets', express.static('/assets')); 
server.use('/css', express.static('/css')); 
server.listen(8080);